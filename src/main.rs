#![feature(proc_macro_hygiene, decl_macro, concat_idents, drain_filter, uniform_paths, type_alias_enum_variants)]
#![recursion_limit = "4096"]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate maplit;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate serde_json;
#[macro_use]
extern crate mashup;
#[macro_use]
extern crate cached;
#[macro_use]
extern crate bcrypt;
#[macro_use]
extern crate lazy_static;

use rocket::request::Form;
use rocket_contrib::serve::StaticFiles;
use rocket_contrib::templates::Template;
pub use std::collections::HashMap;
use rocket::response::status::{NotFound, Accepted};
use rocket::http::{Cookie, Cookies};
use std::time::{Duration, Instant};
use std::thread::sleep;
use std::sync::Mutex;

pub mod file;
pub mod user {
    pub mod repo;
    pub mod traits;
    pub mod models;
    pub mod enums;
}

pub mod wow {
    mod function;
    pub use function::*;

    pub mod repo;
    pub mod datastore;
    pub mod model;
    pub mod build;
}

use user::enums::*;
use user::models::*;
use wow::model::Ability;
use wow::build::BuildInfo;

#[post("/create-build", data = "<build>")]
fn create_build(user: User, build: Form<Build>) {
    user::repo::get().add_build_to_user(user, build.into_inner());
}

lazy_static! {
    static ref EMPTY_BUILD: BuildInfo = wow::build::get(Build {
        link: "::".to_owned(),
        name: "N.A".to_owned(),
        description: "N.A".to_owned(),
        short_description: "N.A".to_owned()
    });

    static ref MAIN_BUILDS: Vec<BuildInfo> = get_all_builds();
}


#[get("/new-build")]
fn new_build(user: User) -> Template {
    let mut build = EMPTY_BUILD.clone();
    build.set_user(Some(user));
    Template::render("new_build", build)
}

#[get("/new-build/<link>")]
fn load_build(user: User, link: String) -> Template {
    // TODO: Reintroduce link here
    let mut build = EMPTY_BUILD.clone();
    build.set_user(Some(user));
    Template::render("new_build", build)
}

#[get("/build/<username>/<link>")]
fn build(user: Option<User>, username: String, link: String) -> Template {
    let build = user::repo::get().get_build(username, link);
    match build {
        Some(t) => {
            let mut build_info = wow::build::get(t);
            build_info.set_user(user);
            Template::render("build", build_info)
        },
        None => Template::render(
            "build_missing",
            hashmap!("username_or_empty" => user.map(|u| u.username).unwrap_or("".to_owned()))
        )
    }
}

// TODO: Refactor String to Result
#[post("/users/new", data = "<user>")]
fn new_user(mut cookies: Cookies, user: Form<NewUser>) -> String {
    let repo = user::repo::get();
    let result = repo.create(user.username.to_owned(), user.password.to_owned());

    match result {
        UserCreateResult::Success(model) => {
            cookies.add_private(Cookie::new("user_id", format!("{}", model.id)));
            return "Success".to_owned()
        },
        UserCreateResult::Failure(reason) => match reason {
            UserCreateFailure::UsernameNotUnique => "Failure::UsernameNotUnique",
            UserCreateFailure::UsernameTooLong => "Failure::UsernameTooLong"
        }
    }.to_owned()
}

#[post("/login", data = "<user>")]
fn login(mut cookies: Cookies, user: Form<NewUser>) -> String {
    let repo = user::repo::get();
    let result = repo.get(user.username.to_owned(), user.password.to_owned());

    match result {
        None => "Failure::UserNotFound",
        Some(user) => {
            cookies.add_private(Cookie::new("user_id", format!("{}", user.id)));
            "Success"
        }
    }.to_owned()
}

#[post("/logout")]
fn logout(_user: User, mut cookies: Cookies) -> Accepted<String> {
    cookies.remove_private(Cookie::named("user_id"));

    Accepted(Some("Successfully logged out".to_owned()))
}

fn render_template(name: &'static str, user: Option<User>) -> Template {
    Template::render(name, hashmap!(
        "username_or_empty" => user.map(|u| u.username).unwrap_or_default()
    ))
}

#[get("/profile")]
fn profile(user: User) -> Template { render_template("profile", Some(user)) }

#[get("/about")]
fn about(user: Option<User>) -> Template { render_template("about", user) }

#[get("/donate")]
fn donate(user: Option<User>) -> Template { render_template("donate", user) }

#[derive(Serialize)]
struct IndexInfo<'a> {
    builds: &'a Vec<BuildInfo>,
    username_or_empty: String
}

fn get_all_builds() -> Vec<BuildInfo> {
    user::repo::get().get_users().iter()
        .flat_map(|user| {
            user.builds.iter().map(move |build| {
                let mut build = wow::build::get(build.to_owned());
                build.set_user(Some(user.clone()));
                build
            })
        }).collect()
}

#[get("/")]
fn index(user: Option<User>) -> Template {
    let result = Template::render("index", IndexInfo {
        builds: &MAIN_BUILDS,
        username_or_empty: user.map(|u| u.username).unwrap_or("".to_owned())
    });
    result
}

fn main() {
    rocket::ignite()
        .mount("/", routes![
            index, new_build, build, new_user, logout, profile, login,
            create_build, load_build, about
        ])
        .mount("/", StaticFiles::from("static"))
        .attach(Template::fairing())
        .launch();
}

