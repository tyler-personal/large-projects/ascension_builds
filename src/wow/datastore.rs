use std::collections::HashMap;
use crate::wow::model::*;

pub trait Datastore {
    fn get_abilities(&self, kind: &AbilityType) -> Option<Vec<Ability>>;
}

impl Datastore {
    pub fn get_spells_by_id(&self, kind: AbilityType) -> HashMap<String, Ability> {
        self.get_abilities(&kind).unwrap().into_iter().map(|ability: Ability| {
            let id = match kind {
                AbilityType::Spell { .. } => ability.spell_id.to_owned(),
                AbilityType::Talent { .. } => ability.talent_id.to_owned()
            };
            (id, ability)
        }).collect::<HashMap<String, Ability>>()
    }
}