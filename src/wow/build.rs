use crate::wow;
use crate::wow::model::*;
use std::collections::HashMap;
use crate::user::models::{User, Build};
use counter::Counter;

pub fn get(build: Build) -> BuildInfo {
    let link = build.link;
    let repo = wow::repo::get();

    let mut talents_by_id = repo.get_spells_by_id(AbilityType::Talent);
    let mut spells_by_id = repo.get_spells_by_id(AbilityType::Spell);

    let mut talent_ids_to_count: HashMap<String, u8> = HashMap::new();
    let mut spell_ids: Vec<String> = Vec::new();

    for &id in link.split(":").collect::<Vec<&str>>().iter() {
        if id.contains("t") {
            let i = id.find("t").unwrap();
            let result = id.chars().take(i).collect::<String>();
            let count: String = id.chars().skip(i + 1).collect();
            talent_ids_to_count.insert(result, count.parse::<u8>().unwrap());
        } else {
            if id.len() > 0 {
                spell_ids.push(id.to_string());
            }
        }
    }
    let talent_ids: Vec<String> = talent_ids_to_count.clone().into_iter()
        .map(|(id, count)| id)
        .collect();

    let spells: Vec<Ability> = spell_ids
        .iter()
        .map(|id| match spells_by_id.remove(id) {
            Some(x) => x,
            None => wow::get_failed_ability(&id),
        })
        .collect();

    let talents = talent_ids_to_count
        .iter()
        .map(|(id, &count)| Talent {
            ability: match talents_by_id.remove(id) {
                Some(x) => x,
                None => wow::get_failed_ability(&id),
            },
            count,
        })
        .collect();

    // TODO: Probably implement some type of cloning? (I think this would be automatic in higher level langs, wouldn't have to worry about moving)
    // let all_spells_by_id: HashMap<String, Ability> = repo.get_spells_by_id(AbilityType::Spell);
    let all_spells: Vec<Ability> = repo.get_spells_by_id(AbilityType::Spell)
        .into_iter()
        .map(|(_id, ability)| ability)
        .collect();

    // let all_talents_by_id: HashMap<String, Ability> = repo.get_spells_by_id(AbilityType::Talent);
    let all_talents: Vec<Ability> = repo.get_spells_by_id(AbilityType::Talent)
        .into_iter()
        .map(|(_id, ability)| ability)
        .collect();

    macro_rules! get_build {
        ( $( $class:ident ),* ) => {{
            mashup! {
                $(
                    m["all_" $class "_spells"] = all_ $class _spells;
                    m["all_" $class "_talents"] = all_ $class _talents;

                    m[$class "_spells"] = $class _spells;
                    m[$class "_talents"] = $class _talents;
                )*
            }
            m! {
                // TODO: Once I understand recursive TT munching macros, implement that here. Above my understanding currently: https://stackoverflow.com/questions/48856777/simplifying-rust-macro-rules-for-nested-looping
                $(
                    let mut "all_" $class "_spells" = Vec::new();
                    let mut $class "_spells" = Vec::new();
                )*
                let mut other_spells = Vec::new();
                let mut all_other_spells = Vec::new();

                for spell in all_spells {
                    let result: &str = &spell.class[..].to_lowercase();
                    match result {
                        $( stringify!($class) => {
                            ("all_" $class "_spells").push(spell.clone());
                            if (spell_ids.contains(&spell.spell_id)) {
                                ($class "_spells").push(spell);
                            }
                        }, ) *
                        _ => {
                            all_other_spells.push(spell.clone());
                            if (spell_ids.contains(&spell.spell_id)) {
                                other_spells.push(spell);
                            }
                        }
                    }
                }

                $(
                    // TODO: Needed?
                    ("all_" $class "_spells").sort();
                    ("all_" $class "_spells").dedup();
                )*

                $(
                    let mut "all_" $class "_talents" = Vec::new();
                    let mut $class "_talents" = Vec::new();
                )*

                let mut other_talents = Vec::new();
                let mut all_other_talents = Vec::new();

                for spell in all_talents {
                    let result: &str = &spell.class[..].to_lowercase();
                    match result {
                        $( stringify!($class) => {
                            ("all_" $class "_talents").push(spell.clone());
                            if (talent_ids.contains(&spell.talent_id)) {
                                ($class "_talents").push(spell);
                            }
                        }, ) *
                        _ => {
                            all_other_talents.push(spell.clone());
                            if (talent_ids.contains(&spell.talent_id)) {
                                other_talents.push(spell);
                            }
                        }
                    }
                }

                $(
                    ("all_" $class "_spells").sort();
                    ("all_" $class "_spells").dedup();
                    ("all_" $class "_talents").sort();
                    ("all_" $class "_talents").dedup();

                    ($class "_spells").sort();
                    ($class "_spells").dedup();
                    ($class "_talents").sort();
                    ($class "_talents").dedup();
                )*

                let counts = spells.iter()
                    .map(|spell| spell.class.to_owned())
                    .collect::<Counter<_>>()
                    .most_common_ordered();

                BuildInfo {
                    name: build.name,
                    description: build.description,
                    short_description: build.short_description,
                    username_or_empty: "".to_owned(),
                    link,
                    spells,
                    talents,
                    $( $class "_spells", )*
                    $( $class "_talents", )*
                    $( "all_" $class "_spells", )*
                    $( "all_" $class "_talents", )*
                    other_spells,
                    other_talents,
                    all_other_spells,
                    all_other_talents,
                    first_class: counts.get(0).map(|x| x.0.to_owned()).unwrap_or("".to_owned()),
                    second_class: counts.get(1).map(|x| x.0.to_owned()).unwrap_or("".to_owned()),
                    third_class: counts.get(2).map(|x| x.0.to_owned()).unwrap_or("".to_owned())
                }
            }

        }}
    }

    get_build!(warrior, druid, hunter, mage, paladin, priest, rogue, shaman, warlock)
}

#[derive(Serialize, Deserialize, Clone)]
pub struct BuildInfo {
    name: String,
    link: String,
    description: String,
    short_description: String,
    username_or_empty: String,
    spells: Vec<Ability>,
    talents: Vec<Talent>,

    warrior_spells: Vec<Ability>,
    druid_spells: Vec<Ability>,
    hunter_spells: Vec<Ability>,
    mage_spells: Vec<Ability>,
    paladin_spells: Vec<Ability>,
    priest_spells: Vec<Ability>,
    rogue_spells: Vec<Ability>,
    shaman_spells: Vec<Ability>,
    warlock_spells: Vec<Ability>,
    other_spells: Vec<Ability>,

    warrior_talents: Vec<Ability>,
    druid_talents: Vec<Ability>,
    hunter_talents: Vec<Ability>,
    mage_talents: Vec<Ability>,
    paladin_talents: Vec<Ability>,
    priest_talents: Vec<Ability>,
    rogue_talents: Vec<Ability>,
    shaman_talents: Vec<Ability>,
    warlock_talents: Vec<Ability>,
    other_talents: Vec<Ability>,

    all_warrior_spells: Vec<Ability>,
    all_druid_spells: Vec<Ability>,
    all_hunter_spells: Vec<Ability>,
    all_mage_spells: Vec<Ability>,
    all_paladin_spells: Vec<Ability>,
    all_priest_spells: Vec<Ability>,
    all_rogue_spells: Vec<Ability>,
    all_shaman_spells: Vec<Ability>,
    all_warlock_spells: Vec<Ability>,
    all_other_spells: Vec<Ability>,

    all_warrior_talents: Vec<Ability>,
    all_druid_talents: Vec<Ability>,
    all_hunter_talents: Vec<Ability>,
    all_mage_talents: Vec<Ability>,
    all_paladin_talents: Vec<Ability>,
    all_priest_talents: Vec<Ability>,
    all_rogue_talents: Vec<Ability>,
    all_shaman_talents: Vec<Ability>,
    all_warlock_talents: Vec<Ability>,
    all_other_talents: Vec<Ability>,

    first_class: String,
    second_class: String,
    third_class: String
}

impl BuildInfo {
    pub fn set_user(&mut self, user: Option<User>) {
        self.username_or_empty = user.map(|u| u.username).unwrap_or("".to_owned())
    }
}