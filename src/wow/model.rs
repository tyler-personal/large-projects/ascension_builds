use std::collections::HashMap;

#[derive(Clone, Serialize, Deserialize)]
pub struct Ability {
    pub talent_id: String,
    pub spell_id: String,
    pub class: String,
    pub spec: String,
    pub rank: String,
    pub name: String,
    pub icon_url: String,
    pub description: String,
    pub cost: String,
    pub range:String,
    pub cast_time: String,
    pub level_required: i8,
    pub duration: String,
    pub ability_essence: i8,
    pub talent_essence: i8
}
use std::cmp::Ordering;

impl std::cmp::Ord for Ability {
    fn cmp(&self, other: &Ability) -> Ordering {
        self.name.cmp(&other.name)
    }
}

impl std::cmp::PartialOrd for Ability {
    fn partial_cmp(&self, other: &Ability) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl std::cmp::PartialEq for Ability {
    fn eq(&self, other: &Ability) -> bool {
        self.name == other.name
    }
}

impl std::cmp::Eq for Ability {}

#[derive(Serialize, Deserialize)]
pub enum AbilityType {
    Spell,
    Talent
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Talent {
    pub ability: Ability,
    pub count: u8
}
//
//#[derive(Serialize, Deserialize)]
//pub enum Class {
//    Warrior, Druid, Hunter, Mage, Paladin, Priest, Rogue, Shaman, Warlock, Other
//}
//
//impl std::cmp::Ord for Class {
//    fn cmp(&self, other: &Class) -> Ordering {
//        self.name.cmp(&other.name)
//    }
//}
//
//impl std::cmp::PartialOrd for Class {
//    fn partial_cmp(&self, other: &Class) -> Option<Ordering> {
//        Some(self.cmp(other))
//    }
//}
//
//impl std::cmp::PartialEq for Class {
//    fn eq(&self, other: &Class) -> bool {
//        self.name == other.name
//    }
//}
//
//impl std::cmp::Eq for Class {}
//
//#[derive(Serialize, Deserialize)]
//pub struct DetailedBuild {
//    name: String,
//    abilities_by_class: HashMap<Class, Ability>
//}