use crate::wow::datastore::Datastore;
use crate::wow::model::*;
use crate::file;

pub struct Json {}

impl Datastore for Json {
    fn get_abilities(&self, kind: &AbilityType) -> Option<Vec<Ability>> {
        let is_talent = match &kind {
            AbilityType::Spell { .. } => false,
            AbilityType::Talent { .. } => true
        };

        let file_name = if is_talent { "talents.json" } else { "spells.json" };

        file::get_json(file_name)["abilities"].as_array().map(|arr| {
            arr.iter().map(|ability| {
                let range_text = ability["range"].as_str().unwrap();
                let range_token = range_text.find("(").or(Some(range_text.len())).unwrap();
                let get_str = |v: &str| { ability[v].as_str().unwrap().to_string() };

                let cost = get_str("cost");
                let cast_time = get_str("castTime");
                let level_required = get_str("levelRequired").parse::<i8>().unwrap();

                let is_spell = match(cost != "None", cast_time != "Instant", is_talent) {
                    (_, true, true) => true,
                    (true, _, true) => true,
                    (_, _, false) => true,
                    _ => false
                };

                let ability_essence = match(is_spell, level_required == 60) {
                    (true, true) => 3,
                    (true, false) => 2,
                    _ => 0
                };
                let talent_essence = if is_talent { 1 } else { 0 };
                Ability {
                    name: get_str("name"),
                    talent_id: get_str("talentID"),
                    spell_id: get_str("spellID"),
                    class: get_str("class"),
                    spec: get_str("spec"),
                    level_required,
                    range: range_text.chars().take(range_token).collect(),
                    cost,
                    duration: get_str("duration"),
                    cast_time,
                    icon_url: get_str("iconURL"),
                    description: get_str("description").replace(". ", "\\n"),
                    rank: get_str("rank"),
                    ability_essence,
                    talent_essence
                }
            }).collect()
        })
    }
}
static JSON_REPO: Json = Json { };

pub fn get() -> &'static Datastore { &JSON_REPO }