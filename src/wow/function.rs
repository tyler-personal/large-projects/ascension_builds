use crate::wow::model::Ability;
pub fn get_failed_ability(id: &str) -> Ability {
    Ability {
        spell_id: "13513u32".to_string(),
        name: "Missing Ability".to_string(),
        rank: "N.A".to_string(),
        cost: "N.A".to_string(),
        range: "N.A".to_string(),
        cast_time: "N.A".to_string(),
        spec: "N.A".to_string(),
        talent_id: "N.A".to_string(),
        description: format!("Your ability wasn't found with the following ID: {}", id).to_string(),
        class: "N.A".to_string(),
        icon_url: "https://wow.zamimg.com/images/wow/icons/large/ability_creature_cursed_02.jpg"
            .to_string(),
        duration: "N.A".to_string(),
        level_required: 0,
        ability_essence: 0,
        talent_essence: 0
    }
}

pub fn get_main() -> Ability {
    Ability {
        spell_id: "13513u32".to_string(),
        name: "Ascension Builds".to_string(),
        rank: "Rank 666".to_string(),
        cost: "180% total mana".to_string(),
        range: "600 yd range".to_string(),
        cast_time: "3.2 seconds".to_string(),
        spec: "Fire".to_string(),
        talent_id: "13481".to_string(),
        description: "Launches a bolt of monkey shit at the enemy, causing 18 to 20 smelly damage and inflicting MASSIVE dysentery".to_string(),
        class: "Mage".to_string(),
        icon_url: "https://wotlk.evowow.com/static/images/wow/icons/large/spell_frost_frostbolt02.jpg".to_string(),
        duration: "14 seconds".to_string(),
        level_required: 0,
        ability_essence: 0,
        talent_essence: 0
    }
}