use crate::user::models::User;
pub enum UserCreateResult {
    Success(User),
    Failure(UserCreateFailure)
}

pub enum UserCreateFailure {
    UsernameNotUnique, UsernameTooLong
}