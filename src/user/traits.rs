use crate::user::models::*;
use crate::user::enums::*;

pub trait Datastore {
    fn get_users(&self) -> Vec<User>;
    fn get(&self, username: String, password: String) -> Option<User>;
    fn get_by_id(&self, id: u16) -> Option<User>;

    fn get_build(&self, username: String, link: String) -> Option<Build>;

    fn create(&self, username: String, password: String) -> UserCreateResult;
    fn add_build(&self, username: String, password: String, build: Build);
    fn add_build_to_user(&self, user: User, build: Build);

    fn get_users_without(&self, without: &mut User) -> Vec<User> {
        self.get_users().drain_filter(|u| u != without).collect()
    }
}
