use crate::user::traits::Datastore;

use crate::file;
use bcrypt::{hash, verify, DEFAULT_COST};
use serde_json;
use crate::user::models::{User, Build};
use crate::user::enums::*;

pub struct Json {}

fn get_hash(password: &str) -> String {
    hash(password, 4).unwrap()
}

impl Datastore for Json {
    fn get_users(&self) -> Vec<User> {
        let data = file::get_str("users.json");
        serde_json::from_str(&data).unwrap()
    }

    fn get(&self, username: String, password: String) -> Option<User> {
        self.get_users().into_iter().find(|u| {
            // let hashed = get_hash(&password[..]);
            if u.username != username {
                return false;
            }
            let valid = verify(&password[..], &u.password_hash).unwrap_or(false);
            println!("{}", valid);
            return valid
        })
    }

    fn get_by_id(&self, id: u16) -> Option<User> {
        self.get_users().into_iter().find(|u| u.id == id)
    }

    fn get_build(&self, username: String, link: String) -> Option<Build> {
        self.get_users().into_iter()
            .find(|user| user.username == username)
            .and_then(|user| user.builds.into_iter().find(|build| build.link == link))
    }

    fn create(&self, username: String, password: String) -> UserCreateResult {
        if username.len() > 30 {
            return UserCreateResult::Failure(UserCreateFailure::UsernameTooLong)
        }

        let mut users = self.get_users();

        if users.iter().map(|user| &user.username).any(|name| name == &username) {
            return UserCreateResult::Failure(UserCreateFailure::UsernameNotUnique { })
        }

        let user = User {
            id: users.len() as u16 + 1,
            username,
            password_hash: get_hash(&password[..]),
            builds: Vec::new(),
        };
        let result = user.clone();

        users.append(&mut vec![user]);
        file::overwrite_file("users.json", &users);
        UserCreateResult::Success(result)
    }

    fn add_build_to_user(&self, mut user: User, build: Build) {
        let mut new_builds: Vec<Build> = vec![build];
        let mut old_builds: Vec<Build> = user.builds;
        old_builds.append(&mut new_builds);
        user.builds = old_builds;
        let mut users: Vec<User> = self.get_users_without(&mut user);
        println!("{}", users.len());
        users.append(&mut vec![user]);
        file::overwrite_file("users.json", &users);
    }
    fn add_build(&self, username: String, password: String, build: Build) {
        let mut user = self.get(username, password).expect("User not found");
        self.add_build_to_user(user, build);
    }
}
static JSON_REPO: Json = Json { };

pub fn get() -> &'static Datastore {
    &JSON_REPO
}