use std::cmp::Ordering;
use rocket::request::{FromRequest, Outcome, Request};
use crate::user::repo;

#[derive(Serialize, Deserialize, Clone)]
pub struct User {
    pub id: u16,
    pub username: String,
    pub password_hash: String,
    pub builds: Vec<Build>,
}

impl std::cmp::Ord for User {
    fn cmp(&self, other: &User) -> Ordering {
        self.username.cmp(&other.username)
    }
}
impl std::cmp::PartialOrd for User {
    fn partial_cmp(&self, other: &User) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
impl std::cmp::PartialEq for User {
    fn eq(&self, other: &User) -> bool {
        self.username == other.username && self.password_hash == other.password_hash
    }
}
impl std::cmp::Eq for User {}

impl<'a, 'r> FromRequest<'a, 'r> for User {
    type Error = ();
    fn from_request(request: &'a Request<'r>) -> Outcome<User, ()> {
        let user_id: Option<String> = request.cookies().get_private("user_id")
            .and_then(|cookie| cookie.value().parse().ok());

        user_id
            .and_then(|id| id.parse::<u16>().ok())
            .and_then(|id| repo::get().get_by_id(id))
            .map(|user| Outcome::Success(user))
            .unwrap_or(Outcome::Forward(()))
    }
}

#[derive(Serialize, Deserialize, Clone, FromForm)]
pub struct Build {
    pub name: String,
    pub link: String,
    pub description: String,
    pub short_description: String
}

#[derive(FromForm)]
pub struct NewUser {
    pub username: String,
    pub password: String,
}
