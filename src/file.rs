use serde;
use serde_json::Value;
use std::fs;
use std::fs::File;
use std::io::Read;
use std::io::Write;

pub fn get_str(file_name: &str) -> String {
  let mut file: File = File::open(file_name).unwrap();
  let mut data: String = String::new();
  file.read_to_string(&mut data).unwrap();
  data
}
pub fn get_json(file_name: &str) -> Value {
  let data = get_str(file_name);
  serde_json::from_str(&data).unwrap()
}

pub fn get_json_as_array(file_name: &str) -> Vec<Value> {
  get_json(file_name).as_array().unwrap().clone()
}

pub fn overwrite_file<T: serde::Serialize>(file_name: &str, data: &T) {
  let f = File::create(file_name).unwrap();
  f.set_len(0);
  write_file(file_name, data, false);
}

pub fn append_file<T: serde::Serialize>(file_name: &str, data: &T) {
  write_file(file_name, data, true);
}

fn write_file<T: serde::Serialize>(file_name: &str, data: &T, append: bool) {
  let file = fs::OpenOptions::new()
    .write(true)
    .append(append)
    .open(file_name)
    .unwrap();

  writeln!(&file, "{}", serde_json::to_string(data).unwrap());
}
