String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};
document.body.innerHTML = document.body.innerHTML.replace(/\\n/g, '<br/><br/>');
let addSpellsMode = false;
let addTalentsMode = false;
let level = 60;

const allAbilities = $('.talent__block');

const addedSpells = () => $("#spells-column .talent__block").toArray();
const addedTalents = () => $("#talents-column .talent__block").toArray();
const addedAbilities = () => [...addedSpells(), ...addedTalents()];

const classes = ["warrior", "hunter", "mage", "paladin", "priest", "rogue", "shaman", "warlock", "druid", "other"];
const allClasses = classes.map(e => `${e}_spells`).concat(classes.map(e => `${e}_talents`));
allClasses.forEach(str => {
    const btn = $(`#${str}-btn`);
    btn.text(btn.text().split("_").map(e => e.capitalize()).join(" "));
    btn.on("click", function() {
        const spells = $(`#${str}-spells`);
        if (!spells.transition("is animating"))
            spells.transition({animation: "slide down", duration: '.5s'})
    });
});

function removeAbilityBlock(abilityBlock) {
    [abilityBlock.nextSibling.nextSibling, abilityBlock.nextSibling, abilityBlock].forEach(e => e.remove());
}

$("#submit-options").click(function() {
    const oldLevel = level;
    level = parseInt($("#level").val());
    const toBeRemoved = addedAbilities().filter(e => parseInt(e.getAttribute("data-level")) > level);
    $("#options-modal").modal("hide");
    if (toBeRemoved.length > 0) {
        swal({
            text: `${toBeRemoved.length} spells/talents will be removed due to level restrictions.\nDo you want to continue?`,
            icon: "warning",
            buttons: {
                cancel: true,
                confirm: {
                    text: "Proceed",
                    closeModal: true,
                    className: "ui button inverted green"
                }
            }

        }).then(function(confirmed) {
            if (confirmed) {
                toBeRemoved.forEach(e => removeAbilityBlock(e));
                runSpellDropdownSetup();
                if (level < 10)
                    swal({
                        text: "Spells from level 1-10 are not level restricted.\nThis means a level 1 can use a level 7 spell.",
                        icon: "info"
                    })
            } else {
                level = oldLevel;
            }
        });
    }
});

function setupTransition(btn, block, other, semanticAnimation, animation, otherAnimation, allBlock, allAnimation, blockOnClick) {
    btn.on({ click: function() {
        blockOnClick();
        other.removeClass(`${otherAnimation}-back`);
        other.removeClass(otherAnimation);
        other.transition({
            animation: semanticAnimation,
            duration: '1s',
            onComplete: function() {
                ["green", "blue"].forEach(e => btn.toggleClass(e));
                const textChild = btn.children().first();
                const iconChild = $(btn.children()[1]).children().first();
                textChild.text(textChild.text() === "Add/Remove" ? "Back" : "Add/Remove");
                ["right", "left"].forEach(e => iconChild.toggleClass(e));
                btn.blur();
            }
        });

        allBlock.transition({ animation: allAnimation, duration: '1s' });

        if (block.hasClass(animation)) {
            block.removeClass(animation);
            block.addClass(`${animation}-back`);
        } else {
            block.removeClass(`${animation}-back`);
            block.addClass(animation);
        }
    }})
}
$(".count-label").each(function() {
    if ($(this).text() === 'x')
        $(this).addClass("hidden");
});

let spellsBtn = $("#spells-change-button");
let talentsBtn = $("#talents-change-button");
let spellsBlock = $("#spells-column");
let talentsBlock = $("#talents-column");
let allSpellsBlock = $("#all-spells-column");
let allTalentsBlock = $("#all-talents-column");
setupTransition(spellsBtn, spellsBlock, talentsBlock, "slide left", "spells-animate", "talents-animate", allSpellsBlock, "slide right", function() { addSpellsMode = !addSpellsMode; });
setupTransition(talentsBtn, talentsBlock, spellsBlock, "slide right", "talents-animate", "spells-animate", allTalentsBlock, "slide left", function() { addTalentsMode = !addTalentsMode; });

function setupSpellDropdown(spellBlock, spellInfo) {
    let target = spellInfo;

    let iconBlock = $(spellBlock.children()[1]);
    let imgBlock = $(spellBlock.children()[2]);

    let name = imgBlock.attr("alt");

    const requiredLevel = spellBlock.data("level") < 10 ? 1 : spellBlock.data("level");

    const setup = (color, error_level) => {
        spellBlock.css("color", color);
        spellBlock.next().find("#levelRequired").css("color", color);
        spellBlock.data("error", error_level);
    };

    if (level < requiredLevel)
        setup("#9f3a38", "level");
    else
        setup("#fff", "");


    let tt = e => target.transition(e);
    let preventDropdown = false;
    iconBlock.on("click", function(e) {
        preventDropdown = true;
        if (spellBlock.data("error") === "level") {
            swal({
                text: `Your Level: ${level}\n Level Required: ${spellBlock.data("level")}`,
                icon: 'error'
            });
            return;
        }
        if (addSpellsMode || addTalentsMode) {
            if (iconBlock.hasClass("plus")) {
                let newSpellBlock = spellBlock.clone();
                let newSpellInfo = spellInfo.clone();
                let newIconBlock = $(newSpellBlock.children()[1]);
                newIconBlock.removeClass("plus");
                newIconBlock.addClass("minus");
                let column = addSpellsMode ? "spells" : "talents";
                let names = $(`#${column}-column .talent__block img`).get().map(e => e.alt);
                let isUniqueName = names.every(e => e !== name);

                if (isUniqueName) {
                    let block = addSpellsMode ? spellsBlock : talentsBlock;
                    block.append(newSpellBlock);
                    block.append(newSpellInfo);
                    block.append("<br />");
                    $(newSpellBlock.children()[2]).css("display", "block");

                    newSpellInfo.css("margin", "0");
                    setupSpellDropdown(newSpellBlock, newSpellInfo);
                }

                if (addTalentsMode) {
                    console.log(spellBlock.data("ability-essence"));
                    let talentIsSpell = [2, 3].includes(parseInt(spellBlock.data("ability-essence"), 10));
                    console.log(`talentIsSpell: ${talentIsSpell}`);let id = target.attr("id").split("-")[0];
                    let talent = $($("#talents-column .talent__info").toArray().find(e => e.id.split("-")[0] === id)).prev();
                    if (!talentIsSpell) {
                        let talentCount = talent.find("#talent_count");
                        talentCount.removeClass("hidden");
                        let text = talentCount.text();
                        if (text === "x" || text === "")
                            talentCount.text("x1");
                        else {
                            let num = parseInt(text.slice(1), 10);
                            talentCount.text(`x${num < 5 ? num + 1 : num}`);
                        }
                    }
                }
            } else if (iconBlock.hasClass("minus")) {
                spellBlock.remove();
                spellInfo.next().remove();
                spellInfo.remove();
            }
        }
    });

    spellBlock.on({
        click: () => {
            if (!preventDropdown) {
                if (tt("is visible")) {
                    if (!(tt("is animating"))) {
                       tt({animation: 'drop', duration: '.4s'});
                    }
                }
                else {
                    tt({animation: 'drop', duration: '.5s'})
                }
            }

//                tt({animation: 'drop', duration: tt("is visible") ? tt("is animating") ? '.0s' : '.4s' : '.5s'});
            else
                preventDropdown = false;
        },
        mouseenter: () => { if (addSpellsMode || addTalentsMode) imgBlock.css("display", "none") },
        mouseleave: () => { if (addSpellsMode || addTalentsMode) imgBlock.css("display", "block") }
    });

}
function runSpellDropdownSetup() {
    $.each(allAbilities, function() {
        setupSpellDropdown($(this), $(this).next());
    });
}
runSpellDropdownSetup();

$.each($(".talent__block img"), function() {
    $(this).addClass("add-remove");
});

function determineProgress() {
    classes.forEach(klass => {
        const MAX_SPELL_COUNT = 30;
        const count = $(`.talent__block ui [data-class='${klass}']`).length;
        const progress = count / MAX_SPELL_COUNT;

    });
}

$("#options-btn").on('click', function() {
    $("#options-modal").modal('show');
});

$("#submit-modal-btn").on('click', function() {
    const totalCost = (arr, kind) => arr
        .map(e => e.getAttribute(`data-${kind}-essence`))
        .reduce((acc, e) => acc + parseInt(e, 10), 0);

    const spells = totalCost(addedSpells(), "ability") + totalCost(addedTalents(), "ability");
    const talents = totalCost(addedTalents(), "talent");

    console.log(spells);
    console.log(talents);

    const maxTalents = level - 10 < 0 ? 0 : level - 10;
    const maxEssence = 10 + maxTalents;
    if (spells > maxEssence) {
        swal({text: `
            You have too many spells.\n
            You have ${spells} Ability Essence required by your build.\n
            You have ${maxEssence} available at level ${level}.`, icon: 'error'});
    } else if (talents > maxTalents) {
        swal({text: `
            You have too many talents.\n
            You have ${talents} Talent Essence required by your build.\n
            You have ${maxTalents} available at level ${level}`, icon: 'error'})
    } else {
        $("#submit-modal").modal('show');
    }
});

$("#submit-build").on('click', function() {
    const spells = addedSpells().map(e => e.getAttribute("data-target"));
    const talents = $("#talents-column .talent__block").toArray().map(e => {
        const id = e.getAttribute("data-talent-id");
        const count = $(`#talents-column .talent__block[data-talent-id="${id}"] #talent_count`).first().text().substring(1, 2);
        return `${id}t${count}`;
    });

    $.post(`/create-build`, {
        link: `:${spells.join(":")}:${talents.join(":")}`,
        name: $('#name').val(),
        short_description: $('#short-description').val(),
        description: $('#description').val()
    });
    swal({text: 'Build successfully created!', icon: 'success'}).then(function() { window.location.href = "/" });
});

tracker = {};
function validationOnField(id, submit_id, validation) {
    document.getElementById(id).addEventListener('input', function() {
        let value = this.value.trim();
        let field = $(`#${id}-field`);
        let error = $(`#${id}-error`);
        let submit = $(`#${submit_id}`);

        if (!(submit_id in tracker))
            tracker[submit_id] = [];
        let li = tracker[submit_id];

        if (validation(value) === true) {
            field.addClass("error");
            error.css("display", "block");

            if (!li.includes(id))
                li.push(id);
            if (li.length > 0)
                submit.addClass("disabled");
        } else {
            field.removeClass("error");
            error.css("display", "none");
            tracker[submit_id] = li.filter(e => e !== id);
        }
        if (tracker[submit_id].length < 1)
            submit.removeClass("disabled");
    })
}

function checkLengthOnField(id, maxLength) {
    validationOnField(id, value => value.length > maxLength);
}


console.log("Loaded build.js");

